#include "downloader.h"
#include "ui_downloader.h"

Downloader::Downloader(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::Downloader)
{
	ui->setupUi(this);
	getAllLogs();
	ui->loadingBar->hide();
}

void Downloader::getAllLogs()
{
	manager = new QNetworkAccessManager(this);
	QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this,
					 SLOT(getAllLogsFinished(QNetworkReply *)));
	manager->get(QNetworkRequest(QUrl("https://mydlp-log.sparsetechnology.com/LogsView")));
}

void Downloader::getByFile(QString logSend)
{
	orgManager = new QNetworkAccessManager(this);
	QObject::connect(orgManager, SIGNAL(finished(QNetworkReply*)), this,
					 SLOT(getByFileFinished(QNetworkReply*)));
	auto orgUrl = "https://mydlp-log.sparsetechnology.com/LogsView/GetByFile/" + logSend;
	orgManager->get(QNetworkRequest(QUrl(orgUrl)));
}

void Downloader::getFileContentFromAPI(QString logSend)
{
	apiManager = new QNetworkAccessManager(this);
	QObject::connect(apiManager, SIGNAL(finished(QNetworkReply *)), this,
					 SLOT(getFileContentFromAPIFinished(QNetworkReply *)));
	auto url = "https://mydlp-log.sparsetechnology.com/LogsView/GetRawDataByFile/" + logSend;
	apiManager->get(QNetworkRequest(QUrl(url)));
}

void Downloader::getAllLogsFinished(QNetworkReply *reply)
{
	QByteArray response = reply->readAll();
	if (response.isEmpty())
		return;
	QString str = QString::fromUtf8(response);
	if (str.isEmpty())
		return;
	if (reply->error())
		QMessageBox::information(this, tr("ERROR"), reply->errorString());
	else {
		QJsonDocument jsonDoc = QJsonDocument::fromJson(str.toUtf8());
		if (jsonDoc.isEmpty())
			return;
		QJsonArray array = jsonDoc.array();
		if (array.isEmpty())
			return;
		QMap<QString, QMap<qint64, QString>> logs;
		foreach (const QJsonValue &value, array) {
			QString logFile = value.toString();
			if (logFile.isEmpty())
				return;
			logFile.replace(".log", "");
			logFile.replace("ffff", "");
			QStringList stringList = logFile.split(QRegExp("[_]"), QString::SkipEmptyParts);
			if (stringList.size() != 2)
				continue;
			else 
				return;
			logValue lv;
			lv.ip = stringList[0];
			lv.ts = stringList.at(1).toLongLong();
			logs[lv.ip].insert(lv.ts, value.toString());
		}
		QMapIterator<QString, QMap<qint64, QString>> i(logs);
		while (i.hasNext()) {
			i.next();
			auto ipSend= i.key();
			QMapIterator<qint64, QString> ii(i.value());
			while (ii.hasNext()) {
				ii.next();
				logSend = ii.value();
			}
			auto strDate = QDateTime::fromMSecsSinceEpoch(ii.key()).toString();
			strDate.insert(0, QString("["));
			strDate.insert(25, QString("]"));
			auto dateIp= strDate + ipSend;
			auto dateIpList= dateIp.split(",",QString::SkipEmptyParts);
			for(int i = 0; i <dateIpList.size(); i++) {
				auto item = new QListWidgetItem(dateIpList.at(i));
				item->setToolTip(logSend);
				ui->listWidget->addItem(item);
			}
		}
	}
}

void Downloader::getByFileFinished(QNetworkReply *byFileReply)
{
	QByteArray response = byFileReply->readAll();
	if(response.isEmpty())
		return;
	if (byFileReply->error())
		QMessageBox::information(this, tr("ERROR"), byFileReply->errorString());
	else {
		QString str = QString::fromUtf8(response);
		str.replace("\"","");
		str.replace("\\u0022","\"");
		QJsonDocument doc = QJsonDocument::fromJson(str.toUtf8());
		QJsonObject json = doc.object();
		org.clear();
		if(json.contains("org"))
			org = "ORG : "+json.value("org").toString();
	}
}

void Downloader::getFileContentFromAPIFinished(QNetworkReply *apiReply)
{
	int onlineCount = 0;
	QByteArray response = apiReply->readAll();
	if (response.isEmpty())
		return;
	endpoints.clear();
	QString apiStr = QString::fromUtf8(response);
	if (apiStr.isEmpty())
		return;
	apiStr.replace(QRegExp("\""),"");
	apiStr.replace(QRegExp("\\\\u0022"),"\"");
	apiStr.replace(QRegExp("\\\\"),"/");
	apiStr.replace(QRegExp("////"),"/");
	QJsonDocument doc = QJsonDocument::fromJson(apiStr.toUtf8());
	if (doc.isEmpty())
		return;
	QJsonObject json = doc.object();
	if (json.isEmpty())
		return;
	QJsonValue endpointsValue = json.value("endpoints");
	if (endpointsValue.isNull())
		return;
	if (endpointsValue.type() == QJsonValue::Array) {
		QJsonArray endpointsArray = endpointsValue.toArray();
		if (endpointsArray.isEmpty())
			return;
		for (QJsonValue arr: endpointsArray) {
			QJsonObject obj = arr.toObject();
			if (obj.contains("computer")) {
				columnName computerValue;
				computerValue.comV = obj.value("computer").toString();
				table.computer = computerValue;
			}
			if (obj.contains("ip")) {
				columnName ipValue;
				ipValue.ipV = obj.value("ip").toString();
				table.ip = ipValue;
			}
			if (obj.contains("name")) {
				columnName nameValue;
				nameValue.nameV = obj.value("name").toString();
				table.name = nameValue;
			}
			if (obj.contains("nc_version")) {
				columnName ncValue;
				ncValue.ncV = obj.value("nc_version").toString();
				table.nc_version = ncValue;
			}
			if (obj.contains("os_version")) {
				columnName osValue;
				osValue.osV = obj.value("os_version").toString();
				table.os_version = osValue;
			}
			if (obj.contains("status")) {
				bool statusBool = obj.value("status").toBool();
				table.status.statusV = statusBool ? "online" : "offline";
				if(statusBool == true)
					onlineCount = onlineCount+1;
			}
			if (obj.contains("uuid")) {
				columnName uuidValue;
				uuidValue.uuidV = obj.value("uuid").toString();
				table.uuid = uuidValue;
			}
			if (obj.contains("mydlpclipboard")) {
				QJsonObject dlpclipboardObj = obj.value("mydlpclipboard").toObject();
				Mydlpclipboard mydlpValue;
				mydlpValue.version = dlpclipboardObj.value("version").toString();
				table.mydlpclipboard = mydlpValue;
			}
			if (obj.contains("mydlpfs")) {
				QJsonObject dlpfsObj = obj.value("mydlpfs").toObject();
				Mydlpfs mydlpValue;
				mydlpValue.version = dlpfsObj.value("version").toString();
				table.mydlpfs = mydlpValue;
			}
			if (obj.contains("mydlpmail")) {
				QJsonObject dlpmailObj = obj.value("mydlpmail").toObject();
				Mydlpmail mydlpValue;
				mydlpValue.version = dlpmailObj.value("version").toString();
				table.mydlpmail = mydlpValue;
			}
			if (obj.contains("mydlpprinter")) {
				QJsonObject dlpprinterObj = obj.value("mydlpprinter").toObject();
				Mydlpprinter mydlpValue;
				mydlpValue.version = dlpprinterObj.value("version").toString();
				table.mydlpprinter = mydlpValue;
			}
			if (obj.contains("mydlpweb")) {
				QJsonObject dlpwebObj = obj.value("mydlpweb").toObject();
				Mydlpweb mydlpValue;
				mydlpValue.version = dlpwebObj.value("version").toString();
				table.mydlpweb = mydlpValue;
			}
			endpoints.push_back(table);
		}
	}
	QString stringOnlineCount = QString::number(onlineCount);
	ui->epCountLine->setText(QString::number(endpoints.size()));
	stringOnlineCount = "Online : "+ stringOnlineCount;
	ui->onlineLine->setText(stringOnlineCount);
	ui->orgLine->setText(org);
	TableWidgetDisplay(endpoints);
	if(endpoints.size() == 0) {
		ui->epCountLine->clear();
		ui->onlineLine->clear();
	}
}

void Downloader::TableWidgetDisplay(QVector<MyTableStruct> tableList)
{
	QTableWidget *table = ui->tableWidget;
	table->setColumnCount(12);
	table->setRowCount(tableList.size());
	QStringList upLabels;
	upLabels
			<<"computer"
		   <<"ip"
		  <<"name"
		 <<"nc_version"
		<<"os_version"
	   <<"status"
	  <<"uuid"
	 <<"mydlpclipboard"
	<<"mydlpfs"
	<<"mydlpemail"
	<<"mydlpprinter"
	<<"mydlpweb";
	table->setHorizontalHeaderLabels(upLabels);
	ui->loadingTime->clear();
	QElapsedTimer timer;
	timer.start();
	for(int i=0; i<tableList.size(); i++) {
		ui->tableWidget->setSortingEnabled(false);
		loadingBar();
		table->setStyleSheet(
					"QTableWidget{"
					"background-color: #C0C0C0;"
					"selection-background-color: rgb(52, 101, 164);"
					"}");
		table->setAlternatingRowColors(true);
		table->setSelectionMode(QAbstractItemView::SingleSelection);
		table->setSelectionBehavior(QAbstractItemView::SelectRows);
		auto ep = tableList[i];
		{
			item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.computer.comV));
			table->setItem(i, 0, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.ip.ipV));
			table->setItem(i, 1, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.name.nameV));
			table->setItem(i, 2, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.nc_version.ncV));
			table->setItem(i, 3, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.os_version.osV));
			table->setItem(i, 4, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.status.statusV));
			table->setItem(i, 5, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.uuid.uuidV));
			table->setItem(i, 6, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("Version: %1").arg(ep.mydlpclipboard.version));
			table->setItem(i, 7, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("Version: %1").arg(ep.mydlpfs.version));
			table->setItem(i, 8, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("Version: %1").arg(ep.mydlpmail.version));
			table->setItem(i, 9, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("Version: %1").arg(ep.mydlpprinter.version));
			table->setItem(i, 10, item);
		}
		{
			item = new QTableWidgetItem;
			item->setText(QString("Version: %1").arg(ep.mydlpweb.version));
			table->setItem(i, 11, item);
		}
	}
	ui->loadingBar->hide();
	QString str = QString::number(timer.elapsed());
	ui->loadingTime->setText(str);
	ui->tableWidget->setSortingEnabled(true);
}

void Downloader::drawTable(QString target)
{
	if (target.isEmpty()) {
		QMessageBox::information(this, tr("WARNING"), tr("Filename not empty!"));
		return;
	}
	getFileContentFromAPI(target);
	getByFile(target);
}

void Downloader::on_listWidget_itemClicked(QListWidgetItem *item)
{
	auto file = item->toolTip();
	drawTable(file);
}

void Downloader::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
	Q_UNUSED(previous);
	if(current == nullptr)
		return;
	loadingBar();
	auto file = current->toolTip();
	drawTable(file);
}

void Downloader::on_filterLine_editingFinished()
{
	QTableWidget *table = ui->tableWidget;
	QString filter = ui->filterLine->text();
	for(int i = 0; i<table->rowCount(); i++){
		bool match = true;
		for(int j= 0; j<table->columnCount(); j++){
			QTableWidgetItem *item = table->item(i,j);
			if(item->text().contains(filter)){
				match = false;
				break;
			}
		}
		table->setRowHidden(i,match);
	}
}

void Downloader::loadingBar()
{
	QProgressBar *loadingBar = ui->loadingBar;
	loadingBar->show();
	loadingBar->setValue(0);
	loadingBar->setStyleSheet("QProgressBar:: chunk {    background-color: #2196F3, width: 10px; margin: 0.5px;}");
	loadingBar->setMinimum(0);
	loadingBar->setMaximum(0);
	loadingBar->setValue(100);
}

void Downloader::on_refreshButton_clicked()
{
	ui->listWidget->clear();
	getAllLogs();
}

Downloader::~Downloader()
{
	delete ui;
	delete item;
	delete orgManager;
	delete manager;
	delete apiManager;
}
