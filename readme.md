There were files with log extensions that were constantly updated on a company-owned http link. A network request was sent to the http link and the data on the link was received. The names of the files contained an ip and timestamp value. The names of the files contained an ip and timestamp value. The ip and timestamp values ​​in the name of the file were split and filled into the map. The escape characters of the files with the largest timestamps coming to each ip were deleted, and the files were filtered and transferred to the list widget. According to the selected log file, a network request was sent for the second time in the background. Decode and parse operations were performed on the received data. The data of the selected file has been transferred to the table.

When the program is run;
The user interface opens with a listwidget with ip and an empty tablewidget.
The user chooses the IP from which he wants to receive the data.
(The data of the selected file in the background is decoded and the necessary transformations are made, and parsing is completed.)
The data of the file is reflected in the table.
