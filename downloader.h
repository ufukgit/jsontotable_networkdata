#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QTableWidgetItem>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QElapsedTimer>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class Downloader; }
QT_END_NAMESPACE

class Downloader : public QMainWindow
{
	Q_OBJECT
	
	struct logValue
	{
		QString ip;
		qint64 ts;
	};
	struct columnName
	{
		QString comV, ipV, nameV, osV, ncV, statusV, uuidV, org;
	};
	struct Mydlpclipboard 
	{
		QString ip, uuid, version;
	};
	struct Mydlpfs 
	{
		QString ip, uuid, version;
	};
	struct Mydlpmail 
	{
		QString ip, uuid, version;
	};
	struct Mydlpprinter 
	{
		QString ip, uuid, version;
	};
	struct Mydlpweb 
	{
		QString ip, uuid, version;
	};
	struct MyTableStruct 
	{
		columnName computer, ip, name, nc_version, os_version, status, uuid;
		Mydlpclipboard mydlpclipboard;
		Mydlpfs mydlpfs;
		Mydlpmail mydlpmail;
		Mydlpprinter mydlpprinter;
		Mydlpweb mydlpweb;
	} MyTable;
	QVector<MyTableStruct> endpoints;
	
protected:
	QVector<MyTableStruct> defineRandomData(int indexSize);
	
public:
	Downloader(QWidget *parent = nullptr);
	~Downloader();
	
private slots:
	void getAllLogsFinished(QNetworkReply *reply);
	void getFileContentFromAPIFinished(QNetworkReply *apiReply);
	void getByFileFinished(QNetworkReply *byFileReply);
	void on_listWidget_itemClicked(QListWidgetItem *item);
	void on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);
	void on_filterLine_editingFinished();
	void on_refreshButton_clicked();
	
private:
	Ui::Downloader *ui;
	void TableWidgetDisplay(QVector<MyTableStruct> MyTable);
	void loadingBar();
	void getAllLogs();
	void getFileContentFromAPI(QString logSend);
	void getByFile(QString logSend);
	void drawTable(QString target);
	QString org, logSend;
	MyTableStruct table;
	QTableWidgetItem *item = nullptr;
	QNetworkAccessManager *orgManager = nullptr;
	QNetworkAccessManager *manager = nullptr;
	QNetworkAccessManager *apiManager = nullptr;
};

#endif // DOWNLOADER_H
